#pragma once
#include <Encoder.h>
#include <PID_v1.h>
#include <tuple>

/**
 * This class uses PID to drive a dc motor using PID.
 * Simply modify the set_point to set an angular velocity.
 */
class MotorControl
{
public:
  /**
   * Constructor
   *
   * @param pin the pin driving the motor.
   * @param encoder the encoder object used to get wheel velocity.
   * @param pid_params the proportional, integral, and derivative
   * coefficients to pass to Pid.
   * @see Encoder
   */
  MotorControl(int pin, Encoder encoder, std::tuple<double, double, double> pid_params);

  /**
   * Update the current motor and PID state.
   */
  void update();

  /** The pid object. @see PID */
  PID pid;

  /** The encoder object. */
  Encoder encoder;

  /** The pin driving the motor */
  int pin;

  /** The desired angular velocity. */
  double set_point;
private:

  /** The voltage output by the PID computation. */
  double output;
};


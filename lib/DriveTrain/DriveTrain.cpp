#include "DriveTrain.h"

DriveTrain::DriveTrain(MotorControl left_mc, MotorControl right_mc, double wheel_base, double wheel_rad)
  : left_mc(left_mc)
  , right_mc(right_mc)
  , wheel_base(wheel_base)
  , wheel_rad(wheel_rad)
{}

void DriveTrain::update()
{
  left_mc.update();
  right_mc.update();
}

void DriveTrain::drive(breadbot::DiffDrive diff_drive)
{
  const auto offset = wheel_base * diff_drive.angular / 2;
  left_mc.set_point = (diff_drive.linear - offset) / wheel_rad;
  right_mc.set_point = (diff_drive.linear + offset) / wheel_rad;
}

breadbot::DiffDrive DriveTrain::actual() const
{
  const auto lvel = left_mc.encoder.omega * wheel_rad;
  const auto rvel = right_mc.encoder.omega * wheel_rad;
  breadbot::DiffDrive actual;
  actual.linear = (lvel + rvel) / 2;
  actual.angular = (rvel - lvel) / wheel_base;
  return actual;
}

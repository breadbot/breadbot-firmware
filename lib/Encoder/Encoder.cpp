#include <Arduino.h>
#include "Encoder.h"

Encoder::Encoder(int pin, int ticksPerRev)
  : pin(pin)
  , ticks(0)
  , direction(true)
  , ticks_per_rev(ticksPerRev)
  , prev_millis(0)
  , omega(0)
{
  pinMode(pin, INPUT_PULLUP);
}

long Encoder::getTicks() const
{
  return ticks;
}

void Encoder::reset()
{
  ticks = 0;
  direction = true;
}

void Encoder::tick()
{
  if (direction) ticks++;
  else ticks--;
}

int Encoder::getTicksPerRev() const
{
  return ticks_per_rev;
}

void Encoder::update()
{
  const auto delta_theta = 2 * M_PI * (ticks - prev_ticks) / ticks_per_rev;
  const auto delta_t = (millis() - prev_millis) / 1000.0;

  omega = delta_theta / delta_t;
  prev_millis = millis();
  prev_ticks = ticks;
}

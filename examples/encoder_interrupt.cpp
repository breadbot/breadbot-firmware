#include <Arduino.h>
#include <Encoder.h>

Encoder enc = Encoder(23, 948);

void setup()
{
  Serial.begin(9600);
  attachInterrupt(digitalPinToInterrupt(enc.pin), [] {enc.tick();}, CHANGE);
}

void loop()
{
  enc.update();
  Serial.println("Encoder Position:");
  Serial.println(enc.getTicks());
  Serial.println("Rotational Velocity:");
  Serial.println(enc.getOmega());

  // Make sure the total time for this loop is >= .001 s, or else the time elapsed for the
  // velocity calculation is 0.
  delay(1000);
}

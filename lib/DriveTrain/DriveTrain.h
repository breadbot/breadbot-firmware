#pragma once
#include "breadbot/DiffDrive.h"
#include <MotorControl.h>

/**
 * This class creats a drive train used to execute differential drive
 * operations.
 */
class DriveTrain
{
public:

  /**
   * DriveTrain constructor.
   *
   * @param left_mc the MotorControl object for the left motor.
   * @param right_mc the MotorControl object for the right motor.
   * @param wheel_base the distance between the tracks of the two wheels.
   * @param wheel_rad the radii of the wheels.
   * @see MotorControl
   */
  DriveTrain(MotorControl left_mc, MotorControl right_mc, double wheel_base, double wheel_rad);

  /**
   * Drive at a certain linear and angular velocity.
   */
  void drive(breadbot::DiffDrive diff_drive);

  /**
   * Update the PID state for both wheels.
   */
  void update();

  /**
   * See the actual angular and linear velocities of the drive train.
   * @return the actual angular and linear velocities of the drive train.
   */
  breadbot::DiffDrive actual() const;

  /** The left wheel's motor controller */
  MotorControl left_mc;

  /** The right wheel's motor controller */
  MotorControl right_mc;

private:
  /** The distance between the two wheels. */
  double wheel_base;

  /** The radius of the wheels. */
  double wheel_rad;
};

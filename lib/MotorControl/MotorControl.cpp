#include <Arduino.h>
#include "MotorControl.h"
#include <cmath>

MotorControl::MotorControl(int pin, Encoder encoder, std::tuple<double, double, double> pid_param)
  : pin(pin)
  , encoder(encoder)
  , set_point(0)
  , output(0)
  , pid(&encoder.omega, &output, &set_point, std::get<0>(pid_param), std::get<1>(pid_param), std::get<2>(pid_param), DIRECT)
{
  pinMode(pin, OUTPUT);
  pid.SetMode(AUTOMATIC);
}

void MotorControl::update()
{
  encoder.update();
  pid.Compute();
  analogWrite(pin, output);
}

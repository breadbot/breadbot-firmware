#include <ros.h>
#include <DriveTrain.h>
#include <tf/transform_broadcaster.h>

DriveTrain drive_train(MotorControl(22, Encoder(23,948), std::tuple<double, double, double>(1, 0, 0)),
              MotorControl(24, Encoder(25,948), std::tuple<double, double, double>(1,0,0)),
              .1, .1);

ros::NodeHandle nh;

void callback(const breadbot::DiffDrive& diff_drive)
{
  drive_train.drive(diff_drive);
}

breadbot::DiffDrive actual_vel;

ros::Subscriber<breadbot::DiffDrive> sub("drive", &callback);
ros::Publisher pub("actual_vel", &actual_vel);

void setup()
{
  attachInterrupt(digitalPinToInterrupt(drive_train.left_mc.encoder.pin),
                  [] {drive_train.left_mc.encoder.tick();}, CHANGE);
  attachInterrupt(digitalPinToInterrupt(drive_train.right_mc.encoder.pin),
                  [] {drive_train.right_mc.encoder.tick();}, CHANGE);

  nh.initNode();
  nh.subscribe(sub);
}

void loop()
{
  actual_vel = drive_train.actual();
  pub.publish(&actual_vel);
  drive_train.update();
  nh.spinOnce();
  delay(5);
}

#pragma once

/**
 * This class describes an encoder.
 * It also defines tick() which is to be used in an interrupt.
 * @example encoder_interrupt.cpp
 */
class Encoder
{
public:

  /**
   * Encoder constructor.
   *
   * @param pin the encoder signal pin.
   * @param ticksPerRev the amount of ticks per revolution.
   */
  Encoder(int pin, int ticksPerRev);

  /** The pin used to control the encoder. */
  int pin;

  /**
   * The direction of the motor.
   * false is backwards, true is forwards.
   */
  bool direction;

  /** Get the position of the wheel in ticks.
   *
   * @return the amount of ticks the encoder has rotated.
   */
  long getTicks() const;

  /** Get the amount of ticks per encoder revolution.
   *
   * @return the amount of ticks per encoder revolution.
   */
  int getTicksPerRev() const;

  /** Get the encoder speed in rad/sec.
   *
   * @return the encoder speed in rad/sec
   */

  /** Reset the encoder tick count. */
  void reset();

  /** Update the encoder ticks.
   * This is meant to be used as an isr.
   */
  void tick();

  /** Update the current velocity.
   * This differs from ticks because this updates things such as
   * velocity which are only needed upon observation and hence not
   * attached to interrupt.
   */
  void update();

  /** The encoder's current rotational velocity (rad/sec). */
  double omega;

private:
  /** The amount of ticks the encoder has rotated. */
  long ticks;

  /** The amount of ticks per revolution. */
  const int ticks_per_rev;

  /** The amount of ticks since the last update call. */
  unsigned long prev_millis;

  /** The encoder's amount of ticks as of the previous update call */
  long prev_ticks;
};
